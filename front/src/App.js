
import { HashRouter } from 'react-router-dom';

// import Projects from './screen/projects/Projects';


import React, { useState, Component } from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./assets/css/Star.css";
import logo from "./logo.svg";
import "./App.css";
import Home from "./screen/home/Home";
import Header from "./components/Header/Header";
import { Provider } from "react-redux";
import store from "./screen/home/reduxForm/store";
import Footer from "./components/Footer/Footer";
// import '@coreui/dist/css/coreui.min.css'
import "bootstrap/dist/css/bootstrap.min.css";
import LoginPage from './components/LoginPage';
import { useLocation } from 'react-router-dom';
import SignupPage from './components/SignupPage';
import ForgotPassword from './components/ForgotPassword';
import Howitworks from './components/Howitworks';
import aboutus from './screen/home/components/aboutus';
import benefits from './screen/home/components/benefits';
import integrations from './screen/home/components/ integrations';
import partners from './screen/home/components/partners';
import products from './screen/home/components/products';
import scheduling from './screen/home/components/scheduling';
import estimation from './screen/home/components/estimation';
import visualediting from './screen/home/components/visualediting';
import costalerts from './screen/home/components/costalerts';
import partnersignup from './screen/home/components/partnersignup';


import("typeface-hk-grotesk");



function App(props) {
  // var url = window.location.href;
  // var filename = url.split("/").pop().split("#")[0].split("?")[0];
  // console.log("path_", filename);

  var jwtTokenSet = getCookie('jwt');
  var ls = localStorage.getItem("user");
  // console.log('localstorage 123', ls);
  console.log(window.location.pathname);
  if (window.location.pathname.includes('/login') || window.location.pathname.includes('/signup') || window.location.pathname.includes('/forgotpassword')) {
    return(
      <>
        <Router>
        <Switch>
          <Route path="/login" component={LoginPage} />
          <Route path="/signup" component={SignupPage} />
          <Route path="/forgotpassword" component={ForgotPassword} />
        </Switch>
        </Router>
      </>
    );
  }
  else {
    return (
      <>
        <div className="Front">
          <Provider store={store}>
            {/* <Router basename="/web/p3d/"> */}
            <Router>
              <Header />
              <Switch>
                {/* <Home /> */}
                <Route exact path="/" component={Home} />
                <Route exact path="/howitworks" component={Howitworks} />
                <Route exact path="/aboutus" component={aboutus} />
                <Route exact path="/benefits" component={benefits} />
                <Route exact path="/integrations" component={integrations} />
                <Route exact path="/partners" component={partners} />
                <Route exact path="/aboutus" component={aboutus} />
                <Route exact path="/products" component={products} />
                <Route exact path="/scheduling" component={scheduling} />
                <Route exact path="/estimation" component={estimation} />
                <Route exact path="/visualediting" component={visualediting} />
                <Route exact path="/costalerts" component={costalerts} />
                <Route exact path="/partnersignup" component={partnersignup} />

              
              
              </Switch>
              <Footer />
            </Router>
          </Provider>
        </div>
      </>
    );
  }
}

function getCookie(name) {
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);
  if (begin == -1) {
      begin = dc.indexOf(prefix);
      if (begin != 0) return null;
  }
  else
  {
      begin += 2;
      var end = document.cookie.indexOf(";", begin);
      if (end == -1) {
      end = dc.length;
      }
  }
  // because unescape has been deprecated, replaced with decodeURI
  //return unescape(dc.substring(begin + prefix.length, end));
  return decodeURI(dc.substring(begin + prefix.length, end));
}

export default App;
