import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'
import SignupPage from '../../../components/SignupPage'

export default class partnersignup extends Component {
    
    render() {
        return (
            <>
             <SignupPage  user_type="partner"/>  
            </>
        )
    }
}
