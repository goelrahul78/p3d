import React, { Component } from 'react'
import LoginForm from './LoginForm'
import { ModalBoxPri } from './Modal/ModalBox'
import { NavLink } from 'react-router-dom'
import axios from 'axios'

export default class SignupPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            error: "",
            success: "",
            firstName: "",
            lastName: "",
            email: "",
            mobileNumber: "",
            companyName: "",
            address: "",
            companyType: "",
            industryType: "",
            password: ""
        };
        this.onSubmit = this.onSubmit.bind(this);

        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeMobileNumber = this.onChangeMobileNumber.bind(this);
        this.onChangeCompanyName = this.onChangeCompanyName.bind(this);
        this.onChangeAddress = this.onChangeAddress.bind(this);
        this.onChangeCompanyType = this.onChangeCompanyType.bind(this);
        this.onChangeIndustryType = this.onChangeIndustryType.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onReset = this.onReset.bind(this);
    }

    onChangeFirstName(e) {
        this.setState({firstName: e.target.value});
    }
    onChangeLastName(e) {
        this.setState({lastName: e.target.value});
    }
    onChangeEmail(e) {
        this.setState({email: e.target.value});
    }
    onChangeMobileNumber(e) {
        this.setState({mobileNumber: e.target.value});
    }
    onChangeCompanyName(e) {
        this.setState({companyName: e.target.value});
    }
    onChangeAddress(e) {
        this.setState({address: e.target.value});
    }
    onChangeCompanyType(e) {
        this.setState({companyType: e.target.value});
    }
    onChangeIndustryType(e) {
        this.setState({industryType: e.target.value});
    }
    onChangePassword(e) {
        this.setState({password: e.target.value});
    }
    onReset(e) {
        this.setState({firstName: ""});
        this.setState({lastName: ""});
        this.setState({email: ""});
        this.setState({mobileNumber: ""});
        this.setState({companyName: ""});
        this.setState({address: ""});
        this.setState({companyType: ""});
        this.setState({industryType: ""});
        this.setState({password: ""});
    }
    onSubmit(e){
        e.preventDefault();
        var user_type = 'individual';
        if(this.props.user_type == 'partner'){
            var user_type = 'partner';
        }
        const data = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            mobileNumber: this.state.mobileNumber,
            companyName: this.state.companyName,
            address: this.state.address,
            companyType: this.state.companyType,
            industryType: this.state.industryType,
            password: this.state.password,
            user_type: user_type
        };
        axios.post("/user/add", data)
        .then( (result) => { console.log(result); } )
        .catch( (error) => { console.log(error); } );
    }

    render() {
        return (
            <>
                <section class="section-border border-primary">
                <div class="container d-flex flex-column">
                    <div class="row align-items-center justify-content-center gx-0 min-vh-100">
                    <div class="col-12 col-md-5 col-lg-10 py-8 py-md-11">
                        <h1 class="mb-0 fw-bold text-center">
                        Sign Up
                        </h1>
                        <p class="mb-6 text-center text-muted">
                            Simplify your workflow in minutes.
                        </p>
                        <form class="mb-6" onSubmit={this.onSubmit}>
                        <div class="row">
                       
                           <div class="form-group col-md-6">
                            <label class="form-label" for="first-name">
                            First Name
                            </label>
                            <input type="text" class="form-control" value={this.state.firstname} onChange={this.onChangeFirstName} id="first-name" name="first-name" placeholder="First Name" />
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-label" for="last-name">
                            Last Name
                            </label>
                            <input type="text" class="form-control" value={this.state.lastName} onChange={this.onChangeLastName} id="last-name" name="last-name" placeholder="Last Name" />
                        </div>
                        </div> 
                        <div class="row ">
                        <div class="form-group col-md-6">
                            <label class="form-label" for="email">
                            Email Address
                            </label>
                            <input type="email" class="form-control" value={this.state.email} onChange={this.onChangeEmail} id="email" name="email" placeholder="name@address.com" />
                        </div>
                        <div class="form-group mb-5 col-md-6">
                            <label class="form-label" for="mobile-number">
                            Mobile Number
                            </label>
                            <input type="text" class="form-control" value={this.state.mobileNumber} onChange={this.onChangeMobileNumber} id="mobile-number" name="mobile-number" placeholder="Mobile Number" />
                        </div>
                        <div class="form-group mb-5 ">
                            <label class="form-label" for="password">
                            Password
                            </label>
                            <input type="password" class="form-control" value={this.state.password} onChange={this.onChangePassword} id="password" name="password" placeholder="Password" />
                        </div>
                        </div>
                        <div class="form-group mb-5">
                            <label class="form-label" for="company-name">
                            Company Name
                            </label>
                            <input type="text" class="form-control" value={this.state.companyName} onChange={this.onChangeCompanyName} id="company-name" name="company-name" placeholder="Company Name" />
                        </div>
                        <div class="form-group mb-5">
                            <label class="form-label" for="address">
                            Address
                            </label>
                            <textarea class="form-control" value={this.state.address} onChange={this.onChangeAddress} id="address" name="address" placeholder="Address"></textarea>
                        </div>
                        <div class="row form-group">
                        <div class="form-group mb-5 col-md-6">
                            <label class="form-label" for="company-type">
                            Company Type
                            </label>
                            <select type="text" class="form-control" value={this.state.companyType} onChange={this.onChangeCompanyType} id="company-type" name="company-type">
                            <option value="" disabled>
                          Select..
                        </option>
                        <option value="General Contractor">
                          General Contractor
                        </option>
                        <option value="Sub Contractor">Sub Contractor</option>
                        <option value="Architect">Architect</option>
                        <option value="Design firm">Design firm</option>
                        <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group mb-5 col-md-6">
                            <label class="form-label" for="industry-type">
                            Industry Type
                            </label>
                            <select type="text" class="form-control" value={this.state.industryType} onChange={this.onChangeIndustryType} id="industry-type" name="industry-type">
                               <option value="" disabled>
                    Select..
                  </option>
                  <option value="Residential">Residential</option>
                  <option value="Commercial">Commercial</option>
                  <option value="Industrial">Industrial</option>
                  <option value="Municipal">Municipal</option>
                            </select>
                        </div>
                        </div>
                        <div class="  form-group text-center button-1 ">
                        <button class="btn w-30 btn-primary " type="submit">Sign Up </button> &nbsp; &nbsp;
                        <button type="reset" class="btn btn-outline-secondary clear" onClick={this.onReset}>Clear Values</button>
                        </div>
                        </form>
                        <p class="mb-0 fs-sm text-center text-muted">
                        Already have an account? <NavLink to="/login"> Login </NavLink>
                        </p>
                    </div>
                    </div> 
                </div> 
                </section>
            </>
        )
    }
}
