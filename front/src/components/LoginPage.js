import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import axios from 'axios'

export default class LoginPage extends Component {

    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: "",
            error: ""
        };
        this.emailOnChange = this.emailOnChange.bind(this);
        this.passwordOnChange = this.passwordOnChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e){
        e.preventDefault();
        const data = {
            email: this.state.email,
            password: this.state.password
        };
        axios.post("/auth/login", data)
        .then( (result) => { console.log(result); } )
        .catch( (error) => { console.log(error); } );
    }

    emailOnChange(e){
        this.setState( { email: e.target.value } );
    }
    passwordOnChange(e){
        this.setState( { password: e.target.value } );
    }

    render() {
        return (
            <>
                <section class="section-border border-primary">
                <div class="container d-flex flex-column">
                    <div class="row align-items-center justify-content-center gx-0 min-vh-100">
                    <div class="col-12 col-md-5 col-lg-4 py-8 py-md-11">
                        <h2 class="mb-0 fw-bold text-center">
                       Welcome Back!
                        </h2>
                        <p class="mb-6 text-center text-muted">
                        Users associated with an already existing account can actually login! For real!
                        </p>
                        <form class="mb-6" onSubmit={this.onSubmit}>
                        <div class="form-group ">
                            <label class="form-label" for="email">
                            Email Address
                            </label>
                            <input type="email" class="form-control form-rounded" id="email" placeholder="name@address.com" onChange={this.emailOnChange} value={this.state.email} />
                        </div>
                        <div class="form-group mb-5 ">
                            <label class="form-label" for="password">
                            Password
                            </label>
                            <input type="password" class="form-control form-rounded" id="password" placeholder="Enter your password" onChange={this.passwordOnChange} value={this.state.password} />
                        
                        </div>
                                                  
                        <button class="btn w-100 btn-primary form-rounded" type="submit">
                            Sign in
                        </button>
                        <hr></hr>
                        </form>
                        <p class="mb-0 fs-sm text-center text-muted">
                        Don't have an account yet <NavLink to="/signup"> Signup </NavLink> <br />
                        <NavLink to="/forgotpassword"> Forgot your password? </NavLink>
                        </p>
                    </div>
                    </div> 
                 </div> 
                </section>
            </>
        )
    }
}
