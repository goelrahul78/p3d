import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class ForgotPassword extends Component {
    render() {
        return (
            <>
                <section class="section-border border-primary">
                <div class="container d-flex flex-column">
                    <div class="row align-items-center justify-content-center gx-0 min-vh-100">
                    <div class="col-12 col-md-5 col-lg-4 py-8 py-md-11">
                        <h3 class="mb-0 fw-bold text-center">
                       Forgot  Your Password?
                        </h3>
                        <p class="mb-6 text-center text-muted">
                        Web app service also supports users resetting their password. Email with link to change their password is sent using AWS SES. 
                        </p>
                        <form class="mb-6">
                        <div class="form-group">
                          
                            <input type="email" class="form-control form-rounded"  id="email" placeholder="Enter Email Address..." />
                        </div>
                        
                        <button class="btn w-100 btn-primary form-rounded" type="submit">
                            Reset Password
                        </button>
                        <hr></hr>
                        <hr></hr>
                        </form>
                        <p class="mb-0 fs-sm text-center text-muted">
                        Already have an account?  <NavLink to="/signup"> Login! </NavLink> <br />
                        <NavLink to="/forgotpassword"> Create an Account! </NavLink>
                        </p>
                    </div>
                    </div> 
                </div> 
                </section>
            </>
        )
    }
}
